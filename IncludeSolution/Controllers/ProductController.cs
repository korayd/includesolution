using IncludeSolution.Repositories;
using IncludeSolution.Services;
using Microsoft.AspNetCore.Mvc;

namespace IncludeSolution.Controllers
{
    [Route("api/v1/product")]
    public class ProductController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            var manager = new RepositoryManager(new ProductRepository());
            string query = manager.Run();
            return Ok(query);
        }
    }
}