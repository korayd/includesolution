using IncludeSolution.Repositories;
using IncludeSolution.Services;
using Microsoft.AspNetCore.Mvc;

namespace IncludeSolution.Controllers
{
    [Route("api/v1/branch")]
    public class BranchController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get(string selectParameters, string whereParameters)
        {
            var manager = new RepositoryManager(new BranchRepository(selectParameters, whereParameters));
            string query = manager.RunWithJoinAndWhere();
            return Ok(query);
        }
    }
}