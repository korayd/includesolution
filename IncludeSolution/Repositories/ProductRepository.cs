namespace IncludeSolution.Repositories
{
    public class ProductRepository : TemplateRepository
    {
        public ProductRepository(string selectParameters = null, string whereParameters = null) : base(selectParameters, whereParameters)
        {
        }

        public override void BuildSelect()
        {
            Builder.Append(@"SELECT
                                b.Id,b.Name,b.CityId,b.DistrictId,
                                b.CloseTime as WeekdayCloseTime,b.MinOrderPrice,b.Logo,b.Url,
                                b.Banner,b.OpeningTime as WeekdayOpeningTime,
                                b.SeoTitle,b.SeoDescription,b.WeekendCloseTime,b.WeekendOpeningTime,
                                b.MarketId,b.Integrator,b.DeliveryType,b.OrderQuota,b.NonWorkingDays");

            if (string.IsNullOrEmpty(SelectParameters)) 
                return;
            
            if (SelectParameters.Contains("address"))
            {
                Builder.Append(",b.Longitude,b.Latitude,b.Address ");
            }

            if (SelectParameters.Contains("detail"))
            {
                Builder.Append(",b.IsActive,b.BranchResponsibleId,b.AllowanceRate,b.AllowanceExpiryDay" +
                               ",b.AllowancePassword,b.UserId,b.ApiKey,b.OperationManagerId,b.BranchType, " +
                               "m.Id as Id, m.Name,m.CurrentCode,m.Url, " +
                               "bsr.SmartRank as SmartRank");
            }
        }

        public override void BuildFrom()
        {
            Builder.Append(" from Branch as b ");
        }
    }
}