using System;
using System.Text;

namespace IncludeSolution.Repositories
{
    public abstract class TemplateRepository
    {
        protected readonly string SelectParameters;
        protected readonly string WhereParameters;
        protected readonly StringBuilder Builder;
        public string Query => Builder.ToString();
        
        protected TemplateRepository(string selectParameters, string whereParameters)
        {
            SelectParameters = selectParameters;
            WhereParameters = whereParameters;
            Builder = new StringBuilder();
        }

        public abstract void BuildSelect();
        
        public abstract void BuildFrom();

        public virtual void BuildJoin() => throw new NotImplementedException();

        public virtual void BuildWhere() => throw new NotImplementedException();

        public void Run()
        {
            BuildSelect();
            BuildFrom();
        }
        
        public void RunWithJoin()
        {
            BuildSelect();
            BuildFrom();
            BuildJoin();
        }

        public void RunWithWhere()
        {
            BuildSelect();
            BuildFrom();
            BuildWhere();
        }

        public void RunWithJoinAndWhere()
        {
            BuildSelect();
            BuildFrom();
            BuildJoin();
            BuildWhere();
        }
    }
}