using IncludeSolution.Repositories;

namespace IncludeSolution.Services
{
    public class RepositoryManager
    {
        private readonly TemplateRepository _templateRepository;
        
        public RepositoryManager(TemplateRepository templateRepository) => _templateRepository = templateRepository;
        
        public string RunWithJoinAndWhere()
        {
            _templateRepository.RunWithJoinAndWhere();
            return _templateRepository.Query;
        }
        
        public string Run()
        {
            _templateRepository.Run();
            return _templateRepository.Query;
        }

        public string RunWithJoin()
        {
            _templateRepository.RunWithJoin();
            return _templateRepository.Query;
        }

        public string RunWithWhere()
        {
            _templateRepository.RunWithWhere();
            return _templateRepository.Query;
        }
    }
}